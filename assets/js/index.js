// Events window

var playerEl = document.getElementById("player");
var documentEl = document.getElementById("document");

var sectionVideo = document.getElementById("videoPlayer");
var sectionDocument = document.getElementById("viewDocument");
var sectionTask = document.getElementById("task");

window.addEventListener('resize', resizePlayer);
window.addEventListener("orientationchange", resizePlayer)

function resizePlayer(event) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    playerEl.width = width;
    documentEl.width = width;
    sectionTask.style.width = width+'px';

    var newHeight = 0;
    newHeight = width >= 992 ? height - 50 : height * 0.35;

    playerEl.height = newHeight;
    documentEl.height = height;
    sectionTask.style.height = newHeight + 'px';
    sectionDocument.style.height = newHeight + 'px';

}

function collapseSection(section, element) {
    var section = document.querySelector('.content-sections[data-section="' + section + '"]');
    section.classList.toggle('hide-section');
    element.classList.toggle('fa-rotate-180');
}

function openMedia(el) {
    media = el.getAttribute('data-media');
    type = el.getAttribute('data-type');

    var sectionActive = el.closest('div.content');

    if (sectionActive.classList.contains('active')) {
        return;
    }

    if (type == 'video') {
        sectionVideo.classList.remove('hide');
        sectionDocument.classList.add('hide');
        sectionTask.classList.add('hide');

        playerEl.setAttribute('src', media);


    } else if (type == 'document') {

        sectionDocument.classList.remove('hide');
        sectionVideo.classList.add('hide');
        sectionTask.classList.add('hide');

        documentEl.setAttribute('src', media);

    } else if (type == 'task') {
        sectionDocument.classList.add('hide');
        sectionVideo.classList.add('hide');
        sectionTask.classList.remove('hide');
    } else {

    }

    removeActive();
    sectionActive.classList.add('active');

}

function activeResources(el) {
    var contentResources = el.closest('div.content-resource');
    var resources = contentResources.querySelector('.popup');
    console.log(resources);

    resources.classList.toggle('pupup-active');
}

function removeActive() {
    var sectionsActive = document.querySelectorAll('div.content.active');

    for (var i = 0; i < sectionsActive.length; i++) {
        sectionsActive[i].classList.remove('active');
    }

}

function readDocument(){
    sectionDocument.classList.add('page-float');
}
function solvedTask(){
    sectionTask.classList.add('page-float');
}

function closePage(){
    var pageFloat = document.querySelectorAll('div.page-float')[0];
    pageFloat.classList.remove('page-float');
}

//Events Drag and drop 

var fileDrop = document.getElementById("fileLabel");
var fileEl = document.getElementById("fileInput");
var droppedFiles;
var files = [];

fileEl.addEventListener('change', function (e) {
    console.log('oiga');
    addFiles(fileEl.files);
    fileEl.value = '';
});

function overrideDefault(event) {
    event.preventDefault();
    event.stopPropagation();
}

function fileHover() {
    fileDrop.classList.add('file-hover');
}

function fileHoverEnd() {
    fileDrop.classList.remove('file-hover');
}

function saveFile(event) {
    droppedFiles = event.dataTransfer.files;
    addFiles(droppedFiles);
}

function addFiles(f) {
    for (var i = 0; i < f.length; i++) {
        files.push(f[i])
    }

    showFiles();
}

var filesContent = document.getElementById('filesUpload');

function showFiles() {

    filesContent.innerHTML = '';

    var fragmentElements = document.createDocumentFragment();
    for (var c = 0; c < files.length; c++) {

        var card = document.createElement('div');

        card.classList.add('card-file');
        var id = "'file-" + c + "'"
        card.setAttribute('id', 'file-' + c)


        var template = '<span class="name"> ' + files[c]["name"] + '</span>' + '<button class="button btn btn-icon" onclick="deleteFile(' + id + ',' + c + ')"><i class="far fa-trash-alt " ></i></button>';

        card.innerHTML = template;

        fragmentElements.appendChild(card);
    }

    filesContent.appendChild(fragmentElements);
}

function deleteFile(file, position) {
    var fileDel = document.getElementById(file.trim());
    console.log(fileDel);

    fileDel.parentElement.removeChild(fileDel)
    files = files.splice(position, 1);


}

resizePlayer();